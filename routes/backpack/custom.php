<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', config('backpack.base.middleware_key', 'admin'),  'permission:Manage content'],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    Route::crud('page', 'PageCrudController');
    Route::crud('feed', 'FeedCrudController');
    Route::crud('comment', 'CommentCrudController');
    Route::crud('accountfb', 'AccountFBCrudController');
}); // this should be the absolute last line of this file