<?php


namespace App\Core\GraphFB;


use GuzzleHttp\Client;

class GraphFB extends Client
{
    private $version;

    private $token;

    public function __construct(array $config = [], $token = null)
    {
        parent::__construct($config);
        $this->setVersion(config('graphFB.version'));
        $this->setToken($token ?? config('graphFB.access_token'));
    }

    public function setVersion($version) {
        if (in_array($version, config('graphFB.version_support'))) {
            $this->version = $version;
        } else {
            throw new \Exception('We not support this version "?');
        }

        return $this;

    }

    public function graph($uuid, $fields, $param = null, $limit = 0) {
        $uri = $this->createUri($uuid, $param);

        try {
            $response = $this->request('GET',
                $uri,
                [
                    'query' => $this->query($fields, $limit),
                ]
            );
            return $this->getData($response);
        } catch (\Exception $exception) {
            throw new \Exception($exception);
        }
    }

    private function createUri($uuid, $param = null){
        $uri = config('graphFB.uri') . "/" . $this->version . "/" . $uuid;
        if (isset($param)) {
            $uri = $uri . "/$param";
        }

        return $uri;
    }

    private function query($fields, $limit = 0) {
        if (is_array($fields)) {
            $fields = implode(",", $fields);
        }

        $query = [
            'fields' => $fields,
            'access_token' => $this->token,
        ];

        if(!empty($limit)) {
            $query['limit'] = $limit;
        }

        return $query;
    }

    private function getData($response){
        return \GuzzleHttp\json_decode($response->getBody()->getContents());
    }

    public function setToken($token) {
        $this->token = $token;
        return $this;
    }
}
