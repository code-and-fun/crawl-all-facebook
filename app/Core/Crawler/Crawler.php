<?php


namespace App\Core\Crawler;


use App\Core\GraphFB\GraphFB;

class Crawler
{
    public $fields;

    public $id;

    public $token;

    public $exception;

    /**
     * Crawler constructor.
     * @param $id
     * @param $fields
     * @param null $version
     * @param bool $exception
     */
    public function __construct($id, $fields, bool $exception = false, $version = null) {
            $this->setId($id);
            $this->setFields($fields);
            $this->exception = $exception;
    }

    /**
     * Set ID Object Facebook
     * @param $id
     * @return Crawler
     */
    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    /**
     * Set Fields for Graph API
     * @param $fields
     * @return $this
     */
    public function setFields($fields) {
        $this->fields = $fields;
        return $this;
    }

    /**
     * Start Crawl
     * @param string $param
     * @param int $limit
     * @param string $token
     * @return bool|mixed
     * @throws \Exception
     */
    public function startCrawl($param, $limit = 20, $token = null) {
        $graphFB = new GraphFB([], $token);

        try {
            return $graphFB->graph($this->id,
                $this->fields,
                $param,
                $limit
            );

        } catch (\Exception $exception) {

            return $this->failCrawl($exception);
        }
    }

    /**
     * If fail, run this function
     * @param $e
     * @return bool
     * @throws \Exception
     */
    public function failCrawl($e) {
        //do something
        if ($this->exception) throw new \Exception($e);

        return false;
    }


}
