<?php


namespace App\Core\Crawler\Field;


class ImageField implements FieldInterface
{

    public $options = [
        'images'
    ];

    public function getOptions()
    {
        return $this->options;
    }
}
