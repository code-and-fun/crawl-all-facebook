<?php


namespace App\Core\Crawler\Field;


class SimpleField implements FieldInterface
{

    public $options = [
        'id',
        'permalink_url',
        'name',
        'created_time',
        'message',
        'attachments{subattachments.limit(30)}',
        'type'
    ];

    public function getOptions()
    {
        return $this->options;
    }
}
