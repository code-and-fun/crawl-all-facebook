<?php


namespace App\Core\Crawler\Field;


class GroupField implements FieldInterface
{

    public $options = [
        'id',
        'link',
        'permalink_url',
        'name',
        'created_time',
        'message',
        'attachments{media,target,subattachments.limit(40)}',
        'type',
        'likes.limit(0).summary(true)',
        'comments.limit(200){id,created_time,from,message,attachment,comments.limit(100){id,created_time,from,message,attachment}}'
    ];

    public function getOptions()
    {
        return $this->options;
    }
}
