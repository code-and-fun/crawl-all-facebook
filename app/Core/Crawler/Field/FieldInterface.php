<?php


namespace App\Core\Crawler\Field;


interface FieldInterface
{
    public function getOptions();
}
