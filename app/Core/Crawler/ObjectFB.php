<?php


namespace App\Core\Crawler;


use Carbon\Carbon;

class ObjectFB
{
    public static function getID($object) {
        return $object->id;
    }

    public static function getUserName($object) {
        return $object->from->name ?? null;
    }

    public static function getUserId($object) {
        return $object->from->id ?? null;
    }

    public static function getAttachments($object, $min_images = 0) {
        if (isset($object->attachments->data[0]->subattachments)) {
            $attachments = $object->attachments->data[0]->subattachments->data;
        } elseif (isset($object->attachments->data)) {
            $attachments = $object->attachments->data;
        } elseif (isset($object->attachment)) {
            $attachments = [$object->attachment];
        } else {
            $attachments = [];
        }
        if (count($attachments) < $min_images) {
            return false;
        }

        return $attachments;
    }

    public static function getMessage($object) {
        $message = $object->message ?? $object->description ?? $object->attachments->data[0]->description ?? null;
        return nl2br($message);
    }

    public static function getUrl($object) {
        return $object->permalink_url ?? null;
    }

    public static function getLink($object) {
        return $object->link ?? null;
    }

    public static function getImagesId($images) {
        $result = [];
        foreach ($images as $image) {
            $result[] = self::getImageId($image);
        }
        return $result;
    }

    public static function getImages($images) {
        $result = [];
        foreach ($images as $image) {
            $result[] = self::getImageSrc($image);
        }
        return $result;
    }

    public static function getImageId($image) {
        return $image->target->id ?? null;
    }

    public static function getImageSrc($image) {
        return $image->media->image->src ?? null;
    }

    public static function getType($object) {
        return $object->type ?? null;
    }

    public static function getCountLikes($object) {
        return $object->likes->summary->total_count ?? 0;
    }

    public static function getCreatedTime($object) {
        $time = $object->created_time;
        return Carbon::createFromFormat(\DateTime::ISO8601, $time)->addHours(7);
    }

    public static function getComments($object) {
        return $object->comments->data ?? null;
    }

    public static function getMessageTag($comment) {
        return $comment->message_tags ?? null;
    }
}
