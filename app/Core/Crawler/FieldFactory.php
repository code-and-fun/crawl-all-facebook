<?php


namespace App\Core\Crawler;


use App\Core\Crawler\Field\FanpageField;
use App\Core\Crawler\Field\GroupField;
use App\Enum\TypeCrawlerEnum;

class FieldFactory
{
    public static function createField ($type) {
        switch ($type) {
            case TypeCrawlerEnum::TYPE_CRAWL_FANPAGE:
                return new FanpageField();
            case TypeCrawlerEnum::TYPE_CRAWL_GROUP:
                return new GroupField();
            default:
                throw new \Exception('No support this type ');
        }
    }
}
