<?php

namespace App\Jobs;

use App\Enum\TaskStatusEnum;
use App\Models\Image;
use App\services\Image\ImageManager;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Vuh\CliEcho\CliEcho;

class DownloadImage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $image_id;
    public $disk;

    /**
     * Create a new job instance.
     *
     * @param $image_id
     * @param $disk
     */
    public function __construct($image_id, $disk)
    {
        $this->image_id = $image_id;
        $this->disk = $disk;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $image = Image::findOrFail($this->image_id);

        $url = $image->url;

        $image_manager = new ImageManager();
        $download_result = $image_manager->download($url, $this->disk);
        if ($download_result['status']) {
            CliEcho::successnl('Done: ' . $url);

            $image->file_name = $download_result['file_name'];
            $image->disk = $this->disk;
            $image->download_status = TaskStatusEnum::DONE;
            $image->save();
        } else {
            CliEcho::errornl('Fail: ' . $url);
            $image->download_status = TaskStatusEnum::ERROR;
            $image->save();
        }
    }
}
