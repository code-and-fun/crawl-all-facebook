<?php

namespace App\Jobs;

use App\Core\Crawler\Crawler;
use App\Core\Crawler\Field\FanpageField;
use App\Core\Crawler\Field\ImageField;
use App\Core\Crawler\Field\SimpleField;
use App\Core\Crawler\ObjectFB;
use App\Enum\TaskStatusEnum;
use App\Enum\UploadStatusEnum;
use App\Models\Comment;
use App\Models\Feed;
use App\Models\Page;
use App\services\BaseService;
use App\services\Image\ImageManager;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Vuh\CliEcho\CliEcho;
use function Composer\Autoload\includeFile;

class Crawl implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $page_id;
    protected $token;

    protected $task_crawl_comment;
    protected $fields;

    /**
     * Create a new job instance.
     *
     * @param $page_id
     * @param $fields
     * @param bool $crawl_comment
     */
    public function __construct($page_id, $fields, $crawl_comment = true)
    {
        $this->page_id = $page_id;
        $this->task_crawl_comment = $crawl_comment;
        $this->fields = $fields;
    }

    /**
     * Execute the job.
     *
     * @param $token
     * @param $param
     * @param int $limit
     * @param int $min_images
     * @return bool
     * @throws \Exception
     */
    public function handle($token, $param, int $limit = 20, $min_images = 0)
    {
        $this->token = $token;
        CliEcho::infonl('----- Start Crawl Fanpage:' . $this->page_id);

        $page = Page::findOrFail( $this->page_id );
        $page->latest_time_crawl = now();
        $page->setStatusCrawl(TaskStatusEnum::RUNNING);
        $page->save();

        $crawler = new Crawler($page->id_fb, $this->fields, false);

        $data = $crawler->startCrawl($param, $limit, $token);

        if (!$data) {
            $page->setStatusCrawl(TaskStatusEnum::ERROR);
            $page->save();

            return false;
        }

        $data = $data->data;

        foreach ($data as $feed) {
            CliEcho::successnl("-----------------------------------\n");
            CliEcho::infonl($feed_id_fb = ObjectFB::getID($feed));
            CliEcho::info($feed_url =  ObjectFB::getUrl($feed));
            if( $feed_id = BaseService::checkCrawled($feed_id_fb, Feed::class)) {
                CliEcho::error("\t---> Đã crawl\n");
                //Update comment after next
                $this->crawlComments($feed, $feed_id);
                continue;
            }

            CliEcho::success("\t---> Bài mới này :?\n");

            CliEcho::infonl($feed_created_time = ObjectFB::getCreatedTime($feed));


            CliEcho::infonl($feed_link =  ObjectFB::getLink($feed));

            CliEcho::infonl($feed_type =  ObjectFB::getType($feed));

            CliEcho::infonl($feed_message =  ObjectFB::getMessage($feed));

            CliEcho::infonl($feed_like =  ObjectFB::getCountLikes($feed));

            $attachments = ObjectFB::getAttachments($feed, $min_images);


            $new_feed = new Feed([
                'id_fb' => $feed_id_fb,
                'url' => $feed_url,
                'message' => $feed_message,
                'type' => $feed_type,
                'link' => $feed_link,
                'like' => $feed_like,
                'page_id' => $this->page_id,
                'time' => $feed_created_time,
                'task_crawl_status' => TaskStatusEnum::DONE,
            ]);

            if($attachments === false) {
                CliEcho::errornl('Loại vì không đủ điều kiện');
                $new_feed->task_upload_status = UploadStatusEnum::NO;
                $new_feed->save();
                continue;
            } else {
                $new_feed->task_upload_status = UploadStatusEnum::PENDING;
                $new_feed->save();
                $this->crawlImage($attachments, $new_feed->id, Feed::class);
                $this->crawlComments($feed, $new_feed->id);
            }
        }
        $page->setStatusCrawl(TaskStatusEnum::DONE);
        $page->save();
        return true;
    }

    /**
     * Task crawl image
     * @param $ob_images
     * @param $imageable_id
     * @param $imageable_type
     */
    public function crawlImage($ob_images, $imageable_id, $imageable_type) {
        foreach ($ob_images as $ob_image) {
            $image_id = ObjectFB::getImageId($ob_image);
            $height = null;
            $width = null;
            if ($image_id) {
                CliEcho::infonl("Image Id:" . $image_id);
                try {
                    $crawler = new Crawler($image_id, (new ImageField())->getOptions());
                    $data = $crawler->startCrawl(null, null, $this->token)->images[0];
                    $image_url = $data->source;
                    $height = $data->height;
                    $width = $data->width;
                } catch(\Exception $exception) {
                    $image_url = ObjectFB::getImageSrc($ob_image);
                }
                CliEcho::infonl("Image Url:" . $image_url);
                if($image_url) {
                    (new ImageManager())->createImage($image_id, $image_url, $imageable_id, $imageable_type, $width, $height);
                }
            } else {
                CliEcho::errornl("Link not found!");
            }
        }
    }

    /**
     * Task Crawl Comment
     * @param $feed
     * @param $feed_id
     * @param null $parent_id
     */
    public function crawlComments($feed, $feed_id, $parent_id = null) {
        if (!$this->task_crawl_comment) {
            return;
        }
        $comments = ObjectFB::getComments($feed);
        if ($comments) {
            foreach ($comments as $comment) {
                CliEcho::infonl($comment_id = ObjectFB::getID($comment));
                if( $parent = BaseService::checkCrawled($comment_id, Comment::class)) {
                    $this->crawlComments($comment, $feed_id, $parent);
                    continue;
                }
                CliEcho::infonl($comment_created_time = ObjectFB::getCreatedTime($comment));
                CliEcho::infonl($comment_user_id = ObjectFB::getUserId($comment));
                CliEcho::infonl($comment_user_name = ObjectFB::getUserName($comment));

                $comment_message = ObjectFB::getMessage($comment);
                $comment_message_tags = ObjectFB::getMessageTag($comment);

                if ($comment_message_tags) {
                    foreach ($comment_message_tags as $message_tag) {
                        $tag_id = $message_tag->id;
                        $tag_name = $message_tag->name;
                        $comment_message = str_replace($tag_name, "@[$tag_id]", $comment_message);
                    }
                }
                CliEcho::infonl($comment_message);

                $attachments = ObjectFB::getAttachments($comment);


                $new_comment = new Comment([
                   'id_fb' => $comment_id,
                    'fb_user_id' => $comment_user_id,
                    'fb_user_name' => $comment_user_name,
                    'message' => $comment_message,
                    'time' => $comment_created_time,
                    'feed_id' => $feed_id,
                    'parent_id' => $parent_id,
                ]);
                $new_comment->save();
                $this->crawlImage($attachments, $new_comment->id, Comment::class);
                $this->crawlComments($comment, $feed_id, $new_comment->id);
            }
        }
    }

}
