<?php

namespace App\Repositories;

use Backpack\PermissionManager\app\Models\Role;

class RoleRepository extends BaseRepository
{
    public function model()
    {
        return Role::class;
    }

    public function findOrCreate()
    {
        $roleCreated = [];
        foreach (config('backpack.permissionmanager.role') as $role) {
            $roleFound = $this->getByColumn($role, 'name');
            if (!$roleFound) {
                $roleFound = $this->create(['name' => $role, 'guard_name' => 'backpack']);
            }
            $roleCreated[$roleFound->name] = $roleFound->id;
        }
        return $roleCreated;
    }

    public function clearAndCreate()
    {
        $this->deleteAll();
        $roleCreated = [];
        foreach (config('backpack.permissionmanager.role') as $role) {
            $proleFound = $this->create(['name' => $role, 'guard_name' => 'backpack']);
            $roleCreated[$proleFound->name] = $proleFound->id;
        }
        return $roleCreated;
    }

    public function deleteAll()
    {
        foreach ($this->all() as $roleDelete) {
            $this->deleteById($roleDelete->id);
        }
    }
}
