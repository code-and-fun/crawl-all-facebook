<?php

namespace App\Console\Commands;

use App\Core\Crawler\FieldFactory;
use App\Jobs\Crawl;
use App\Models\AccountFB;
use App\Models\Page;
use Illuminate\Console\Command;

class CommandAutoCrawl extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawl:crawl {--type=FANPAGE} {--sleep=10} {--limit=20}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crawl data in FB';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        $type = $this->option('type');
        $limit = $this->option('limit');
        $sleep = (int) $this->option('sleep');

        if ($type == 'FANPAGE') {
            $param = 'posts';
        } else {
            $param = 'feed';
        }

        while (true) {
            \Log::info("Start crawl $type in " . now());
            $acc = AccountFb::firstOrFail();
            $fields = FieldFactory::createField($type)->getOptions();
            Page::where('type', $type)->chunkById(10, function ($pages) use ($limit, $sleep, $acc, $fields, $param) {
                foreach ($pages as $page) {
                    (new Crawl($page->id, $fields))->handle($acc->token, $param, $limit);
                    $this->info("=======================\n=======================");
                    sleep($sleep);
                }
            });
        }
    }
}
