<?php

namespace App\Console\Commands;

use App\Enum\TaskStatusEnum;
use App\Jobs\DownloadImage;
use App\Models\Image;
use Illuminate\Console\Command;

class CommandDownloadImage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'download:image';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command download image';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Image::where('download_status', TaskStatusEnum::DONE)->chunkById(50, function ($images) {
            foreach ($images as $image) {
                (new DownloadImage($image->id, config('crawler.image.disk')))->handle();
            }
        });
    }
}
