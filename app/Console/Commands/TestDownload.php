<?php

namespace App\Console\Commands;

use App\Core\Crawler\Downloader;
use App\Jobs\DownloadImage;
use Illuminate\Console\Command;

class TestDownload extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:download';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command Test Download image';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        (new DownloadImage(1, 'local'))->handle();
    }
}
