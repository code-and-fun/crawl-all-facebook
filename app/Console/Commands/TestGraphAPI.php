<?php

namespace App\Console\Commands;

use App\Core\Crawler\Crawler;
use App\Core\Crawler\Field\SimpleField;
use App\Core\GraphFB\GraphFB;
use App\Jobs\Crawl;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

class TestGraphAPI extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:graph {--page_id=1}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test Graph API Facebook';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        $page_id = $this->option('page_id');
        (new Crawl($page_id))->handle("EAAGNO4a7r2wBABv5jCK3zPZCnRKcIVho118tYZAhFPwMc4mZAFSwTEkZC6h52V1Fi0SRJxaI3EZCAizVfpHCcuDKfvEsfegV4ZBHFEG3u66zLc7G3Wgf7bmXZA2K0EoP2LMt8iCC4KCKnM3ELZBuJsxVkhQZAyN8VET7niClMH0ibgAZDZD");
    }
}
