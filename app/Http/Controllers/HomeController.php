<?php

namespace App\Http\Controllers;

use App\Models\Feed;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function showFeed($id) {
        $feed = Feed::findOrFail($id);
        $comments = $feed->comments()->where('parent_id', null)->get();
        return response()->json([
            $comments
        ], 200);
    }
}
