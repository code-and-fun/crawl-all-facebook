<?php

namespace App\Http\Controllers\Admin;

use App\Enum\UploadStatusEnum;
use App\Http\Requests\FeedRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class FeedCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class FeedCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
//    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Feed');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/feed');
        $this->crud->setEntityNameStrings('feed', 'feeds');
    }

    protected function setupListOperation()
    {
        $this->crud->addColumns([
            [
                'name' => 'id',
                'label' => "ID",
                'type' => 'text'
            ],
            [
                'name' => 'page_id',
                'label' => "Page",
                'type' => 'closure',
                'function' => function($entry) {
                    $page = $entry->page;
                    return "<a href='$page->url'>PAGE</a>";
                }
            ],
            [
                'name' => 'url',
                'label' => "Link FB",
                'type' => 'closure',
                'function' => function($entry) {
                    $url = $entry->url;
                    return "<a href='$url'>Link</a>";
                }
            ],
            [
                'name' => 'type',
                'label' => "Type",
            ],
            [
                'name' => 'fb_user',
                'label' => 'Người đăng',
                'type' => 'closure',
                'function' => function($entry) {
                    $user_name = $entry->fb_user_name;
                    $user_id = $entry->fb_user_id;
                    return "<a href='https://www.facebook.com/$user_id' target='_blank'>$user_name</a>";
                }
            ],
            [
                'name' => 'time',
                'label' => "Thời gian đăng",
                'type' => 'text'
            ],
            [
                'name' => 'created_at',
                'label' => "Thời gian crawl",
                'type' => 'text'
            ],
            [
                'name' => 'public_status',
                'label' => 'Public Status',
                'type' => 'closure',
                'function' => function($entry) {
                    return UploadStatusEnum::getIcon($entry->task_upload_status);
                }
            ],
        ]);

        $this->crud->addFilter([
            'type'  => 'date_range',
            'name'  => 'from_to',
            'label' => 'Time create'
        ],
            false,
            function ($value) { // if the filter is active, apply these constraints
                $dates = json_decode($value);
                $this->crud->addClause('where', 'time', '>=', $dates->from);
                $this->crud->addClause('where', 'time', '<=', $dates->to . ' 23:59:59');
            });

        $this->crud->addFilter([
            'type'  => 'dropdown',
            'name'  => 'filter_status',
            'label' => 'Lọc theo trạng thái'
        ],
            array_flip(UploadStatusEnum::getPossibleEnumValues())
            ,
            function ($value) { // if the filter is active, apply these constraints
                $this->crud->addClause('where', 'task_upload_status', $value );
            });

    }

    protected function setupShowOperation() {

        $this->crud->set('show.setFromDb', false);

        $this->crud->addColumn([
            'name' => 'public_status',
            'label' => 'Public Status',
            'type' => 'closure',
            'function' => function($entry) {
                return UploadStatusEnum::getIcon($entry->task_upload_status);
            }
        ]);

        $this->crud->addColumns([
            [
                'name' => 'url',
                'label' => 'URL',
                'type' => 'closure',
                'function' => function($entry) {
                    $url = $entry->url;
                    return "<a href='$url' target='_blank'>$url</a>";
                }
            ],
            [
                'name' => 'time',
                'label' => 'Thời gian đăng',
                'type' => 'text',
            ],
            [
                'name' => 'fb_user',
                'label' => 'Người đăng',
                'type' => 'closure',
                'function' => function($entry) {
                    $user_name = $entry->fb_user_name;
                    $user_id = $entry->fb_user_id;
                    return "<a href='https://www.facebook.com/$user_id' target='_blank'>$user_name</a>";
                }
            ],
            [
                'name' => 'message',
                'label' => 'Message',
                'type' => 'textarea',
            ],
        ]);

        $this->crud->addColumn([
            'name' => 'images',
            'label' => "Image",
            'type' => 'preview_image',
        ]);
    }


    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
