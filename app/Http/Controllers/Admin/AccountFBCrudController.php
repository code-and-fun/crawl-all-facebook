<?php

namespace App\Http\Controllers\Admin;

use App\Enum\TypeCrawlerEnum;
use App\Http\Requests\AccountFBRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class AccountFBCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class AccountFBCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\AccountFB');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/accountfb');
        $this->crud->setEntityNameStrings('account_fb', 'accounts_fb');
    }

    protected function setupListOperation()
    {
        $this->crud->addColumns([
            [
                'name' => 'email',
                'label' => "Email",
                'type' => 'text'
            ],
            [
                'name' => 'type_crawl',
                'label' => "Loại",
                'type' => 'text'
            ],
            [
                'name' => 'status',
                'type' => 'check',
                'label' => 'Status'
            ],
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(AccountFbRequest::class);

        $this->crud->addFields([
            [
                'name' => 'email',
                'label' => "Email",
                'type' => 'text'
            ],
            [
                'name' => 'url',
                'label' => "Link",
                'type' => 'text'
            ],
            [
                'name' => 'token',
                'label' => "Token",
                'type' => 'text'
            ],
            [
                'name' => 'type_crawl',
                'label' => 'Loại',
                'type' => 'select_enum',
                'enum' => TypeCrawlerEnum::class,
            ]
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
