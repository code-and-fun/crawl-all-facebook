<?php

namespace App\Http\Controllers\Admin;

use App\Enum\TaskStatusEnum;
use App\Enum\TypeCrawlerEnum;
use App\Http\Requests\PageRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class PageCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PageCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Page');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/page');
        $this->crud->setEntityNameStrings('page', 'pages');
    }

    protected function setupListOperation()
    {
        $this->crud->addColumns([
            [
                'name' => 'id', // The db column name
                'label' => "#", // Table column heading
                'type' => 'integer'
            ],
            [
                'name' => 'name', // The db column name
                'label' => "Name", // Table column heading
                'type' => 'text'
            ],
            [
                'name' => 'url', // The db column name
                'label' => "Link Facebook", // Table column heading
                'type' => 'closure',
                'function' => function($entry) {
                    return "<a href='$entry->url'>link</a>";
                }
            ],
            [
                'name' => 'type',
                'label' => "Loại",
                'type' => 'text'
            ],
            [
                'name' => 'latest_time_crawl', // The db column name
                'label' => "Last Time Crawl", // Table column heading
                'type' => 'text',
            ],
            [
                'name' => 'crawl_status',
                'label' => "Crawl Status",
                'type' => 'closure',
                'function' => function($entry) {
                    $value = $entry->task_crawl_status;
                    if($value == -1) {
                        return "<span class='badge badge-error'>FAILED</span>";
                    } elseif ($value == 0) {
                        return "<span class='badge badge-success'>RUNNING</span>";
                    } else {
                        return "<span class='badge badge-primary'>DONE</span>";
                    }
                }
            ],
        ]);

        $this->crud->addFilter([
            'name' => 'filter_status',
            'type' => 'dropdown',
            'label'=> 'Crawl Status'
        ], [
            1 => 'Done',
            0 => 'Running',
            -1 => 'Fail'
        ], function($value) { // if the filter is active
            $this->crud->addClause('where', 'task_crawl_status', $value);
        });

        $this->crud->addFilter([
            'name' => 'Type',
            'type' => 'select2',
            'label'=> 'Loại'
        ], function() {
            return [
                'FANPAGE' => 'FANPAGE',
                'GROUP' => 'GROUP'
            ];
        }, function($value) {
            $this->crud->addClause('where', 'type', $value);
        });
    }

    protected function setupCreateOperation()
    {
        $this->crud->addFields([
            [
                'name' => 'name',
                'label' => "Name",
                'type' => 'text'
            ],
            [
                'name' => 'url',
                'label' => "Link Facebook",
                'type' => 'text'
            ],
            [
                'name' => 'id_fb',
                'label' => "ID on FB",
                'type' => 'text'
            ],
            [
                'name' => 'type',
                'label' => "Loại",
                'type' => 'enum'
            ]
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
