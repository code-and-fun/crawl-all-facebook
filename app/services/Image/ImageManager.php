<?php


namespace App\services\Image;


use App\Models\Image;
use Intervention\Image\ImageManagerStatic;
use Storage;

class ImageManager {

    public function __construct() {

    }

    public function createImage($id, $url, $imageable_id, $imageable_type, $width = null, $height = null) {
        $image = new Image([
            'id_fb' => $id,
            'url' => $url,
            'imageable_id' => $imageable_id,
            'imageable_type' => $imageable_type,
            'width' => $width,
            'height' => $height
        ]);
        $image->save();
        return $image;
    }

    public function download($url, $disk) {

        $file_extension = getExtFB($url);
        $file_name = $this->createFileName($url, $file_extension);
        try {
            $file_image = ImageManagerStatic::make($url);
            Storage::disk($disk)->write( $file_name, $file_image->encode($file_extension, 100) );

            return [
                'status' => true,
                'file_name' => $file_name
            ];
        } catch (\Exception $e) {
            \Log::error('Fail to download: ' . $url . PHP_EOL . $e->getMessage());
            return [
                'status' => false
            ];
        }
    }

    public function createFileName($url, $file_extension) {
        return date('Y/m_d/') . md5($url) . rand(0, 9999) . '.' . $file_extension;
    }


}
