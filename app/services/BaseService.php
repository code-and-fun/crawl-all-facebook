<?php


namespace App\services;


use App\Models\Comment;
use App\Models\Feed;

class BaseService {

    /**
     * Check đã crawl chưa
     * @param $uuid_fb
     * @param $MODEL
     * @return bool
     */
    public static function checkCrawled($uuid_fb, $MODEL) {
        $model = $MODEL::where('id_fb', $uuid_fb)->first();
        if ($model === null) {
            return false;
        } else {
            return $model->id;
        }
    }
}
