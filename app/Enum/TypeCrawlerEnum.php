<?php

namespace App\Enum;


use MyCLabs\Enum\Enum;

class TypeCrawlerEnum extends Enum {

    use ToOptions;

    CONST TYPE_CRAWL_ALL = 'ALL';
    CONST TYPE_CRAWL_FANPAGE = 'FANPAGE';
    CONST TYPE_CRAWL_GROUP = 'GROUP';

    const TEXT = [
        self::TYPE_CRAWL_ALL => "ALL",
        self::TYPE_CRAWL_FANPAGE => "FANPAGE",
        self::TYPE_CRAWL_GROUP => "GROUP",
    ];

    public static function getText($status) {
        return self::TEXT[$status] ?? null;
    }

    public static function getPossibleEnumValues() {
        return array_flip(self::TEXT);
    }

}
