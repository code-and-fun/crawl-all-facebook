<?php

namespace App\Enum;


use MyCLabs\Enum\Enum;

class TaskStatusEnum extends Enum {

    use ToOptions;

    const ERROR = -1;// chạy lỗi
    const RUNNING = 0;// khởi tạo
    const DONE = 1;// chạy xong
}
