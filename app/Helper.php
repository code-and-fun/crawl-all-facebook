<?php

if (!function_exists('trimUrl')) {
    /**
     * @param $url
     * @return string
     */
    function trimUrl($url) {
        if($position = strpos($url, '?')){
            $url = substr($url, 0, $position);
        }
        return $url;
    }
}

if (!function_exists('getExt')) {
    /**
     * @param $url
     * @return string
     */
    function getExt($url) {
        $url = trimUrl($url);
        $path_info = pathinfo($url);
        return $path_info['extension'] ?: 'jpg';
    }
}

if (!function_exists('getExt2')) {
    /**
     * @param $url
     * @return string
     */
    function getExt2($url) {
        if (strpos($url, '.gif') !== false) {
            return 'gif';
        } elseif (strpos($url, '.png') !== false) {
            return 'png';
        } elseif (strpos($url, '.jpg') !== false) {
            return 'jpg';
        } else {
            return 'jpg';
        }
    }
}

if (!function_exists('getExtFB')) {
    /**
     * @param $url
     * @return string
     */
    function getExtFB($url) {
        $ext = getExt($url);
        if ($ext === 'php') {
            $ext = getExt2($url);
        }
        return $ext;
    }
}
