<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Artisan;
use App\Models\BackpackUser as User;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Str;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        if (Role::count() < 3) {
            Artisan::call('data:permission --action=insert');
        }

        $this->createAccount([
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'email_verified_at' => now(),
            'password' => bcrypt('123dok'),
            'remember_token' => Str::random(10),
        ], 'Administrator');

        $this->createAccount([
            'name' => 'admod',
            'email' => 'admod@admod.com',
            'email_verified_at' => now(),
            'password' => bcrypt('123dok'),
            'remember_token' => Str::random(10),
        ], 'Editor');

        $this->createAccount([
            'name' => 'user',
            'email' => 'user@9houz.com',
            'password' => bcrypt('123dok'),
            'remember_token' => Str::random(10),
        ], 'User');
    }

    private function createAccount($dataAccount, $role)
    {
        User::create($dataAccount)->save();

        $accountCreated = User::orderBy('id', 'desc')->first();
        $roleFound = Role::where(['name' => $role])->first();

        if (!$roleFound) {
            throw new \Exception("Role $role doesn't exist!");
        }

        DB::table('model_has_roles')->insert([
            'role_id' => $roleFound->id,
            'model_type' => config('backpack.permissionmanager.models.user'),
            'model_id' => $accountCreated->id,
        ]);
    }
}
