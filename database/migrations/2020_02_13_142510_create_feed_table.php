<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feeds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('uuid')->nullable();
            $table->string('id_fb');
            $table->string('url');
            $table->unsignedInteger('page_id');
            $table->string('fb_user_name')->nullable();
            $table->string('fb_user_id')->nullable();
            $table->text('message')->nullable();
            $table->integer('like')->default(0);
            $table->string('type')->nullable();
            $table->string('link')->nullable()->comment('backlink đến trang khác, link video, link dến ảnh feed');
            $table->dateTime('time');
            $table->integer('task_crawl_status')->default(1)->comment('-1.false - 0.running - 1.done');
            $table->integer('task_upload_status')
                ->default(9)
                ->comment('0.No/1.Running/9.Pending/99.Ready/100.Done/-1.False');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feeds');
    }
}
