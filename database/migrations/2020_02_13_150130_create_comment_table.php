<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('uuid')->nullable();
            $table->string('id_fb');
            $table->unsignedBigInteger('feed_id');
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->string('fb_user_name')->nullable();
            $table->string('fb_user_id')->nullable();
            $table->text('message')->nullable();
            $table->integer('like')->default(0);
            $table->dateTime('time');
            $table->integer('task_upload_status')
                ->nullable()
                ->comment('0.No/1.Running/9.Pending/99.Ready/100.Done/-1.False');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
