<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid')->nullable()->comment('kết nối với nơi upload');
            $table->string('id_fb')->comment('id on facebook');
            $table->string('url')->nullable()->comment('link to facebook');
            $table->string('name');
            $table->string('slug');
            $table->enum('type', ['FANPAGE', 'GROUP']);
            $table->dateTime('latest_time_crawl')->nullable();
            $table->integer('task_crawl_status')->default(1)->comment('-1.false - 0.running - 1.done');
            $table->boolean('switch')->default(0)->comment("1.on - 2.off");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
