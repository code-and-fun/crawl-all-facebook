{{-- checkbox with loose false/null/0 checking --}}
@php
    /** @var TYPE_NAME $entry */
    /** @var TYPE_NAME $column */
    $checkValue = data_get($entry, $column['name']);

    $checkedIcon = data_get($column, 'icons.checked', 'badge-success');
    $uncheckedIcon = data_get($column, 'icons.unchecked', 'badge-error');

    $exportCheckedText = data_get($column, 'labels.checked', trans('backpack::crud.yes'));
    $exportUncheckedText = data_get($column, 'labels.unchecked', trans('backpack::crud.no'));

    $icon = $checkValue == false ? $uncheckedIcon : $checkedIcon;
    $text = $checkValue == false ? $exportUncheckedText : $exportCheckedText;
@endphp

    <span class="badge {{ $icon }}">{{ $text }}</span>

