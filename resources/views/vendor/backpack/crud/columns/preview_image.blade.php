@php
    /** @var TYPE_NAME $entry */
    $images =json_decode($entry->images);
@endphp
<div class="row">
    @if($images !== null && count($images))
        @foreach($images as $image)
            <div class="col-3 mt-3">
                <img src="{{url($image->disk . '/'. $image->file_name)}}" onerror = "this.onerror = null; this.src = '{{$image->url}}'; " alt="" style="max-width: 100%">
            </div>
        @endforeach
    @else
            <span class="text-error">Not image preview</span>
    @endif
</div>


