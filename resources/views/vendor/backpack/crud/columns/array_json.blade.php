{{-- enumerate the values in an array  --}}
@php
    $values = data_get($entry, $column['name']);
    $suffix = $column['suffix'] ?? '';

    // the value should be an array wether or not attribute casting is used
    if (!is_array($values)) {
        $values = json_decode($values, true);
    }
@endphp

<span>
    @php
        if ($values && count($values) && count($values[0])) {
            $result = '';
            foreach ($values as $value) {
                $value = $value[$column['name']];
                $result .= "<span class=\"badge badge-primary\">$value $suffix</span>";
            }
            echo $result;
        } else {
            echo '-';
        }
    @endphp
</span>
