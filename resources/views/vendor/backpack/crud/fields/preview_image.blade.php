@php
    /** @var TYPE_NAME $entry */
    $images =json_decode($entry->images);
@endphp
<div @include('crud::inc.field_wrapper_attributes') >
<div class="row">
    @if($images !== null && count($images))
        @foreach($images as $image)
            <div class="col-4 mt-4">
                <img src="{{url('image_project/'. $image->file_name)}}" onerror = "this.onerror = null; this.src = '{{$image->url}}'; " alt="" style="max-width: 100%">
            </div>
        @endforeach
    @else
            <span class="text-error">Not image preview</span>
    @endif
</div>
</div>


