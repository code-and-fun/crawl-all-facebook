<!-- enum -->
<div @include('crud::inc.field_wrapper_attributes') >
    <div>
        <label>{!! $field['label'] !!}</label>
        @include('crud::inc.field_translatable_icon')
    </div>
    @php
        /** @var TYPE_NAME $field */
        $enum = $field['enum'];
        $possible_values = $enum::getPossibleEnumValues();
        $optionPointer = 0;
    @endphp

    @if((isset($field['value']) && $field['value'] == 100))
        <span class="badge badge-success">DONE</span>
    @elseif($possible_values == (array)$possible_values)

        @foreach ($possible_values as $key => $value )
            @php ($optionPointer++)

            @if($value == 100) @break @endif


                <div class="form-check form-check-inline pr-4">
                    <input  type="radio"
                            style="width: 15px;height: 15px"
                            class="form-check-input"
                            id="{{$field['name']}}_{{$optionPointer}}"
                            name="{{$field['name']}}"
                            value="{{$value}}"
                        @include('crud::inc.field_attributes')
                            @if (( old(square_brackets_to_dots($field['name'])) &&  old(square_brackets_to_dots($field['name'])) == $value) || (isset($field['value']) && $field['value']==$value))
                            checked
                        @endif
                    >
                    <label class="radio-inline form-check-label font-weight-normal" for="{{$field['name']}}_{{$optionPointer}}"
                    style="position: relative;top: -1px"
                    >{!! $key !!}</label>
                </div>
        @endforeach

    @endif

    {{-- HINT --}}
    @if (isset($field['hint']))
        <p class="help-block">{!! $field['hint'] !!}</p>
    @endif
</div>
