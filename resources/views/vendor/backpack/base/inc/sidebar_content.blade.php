<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>

<!-- Users, Roles, Permissions -->
@if(
    backpack_user()->can('Create users') ||
    backpack_user()->can('Edit users') ||
    backpack_user()->can('Delete users') ||
    backpack_user()->can('Change user passwords')
)
    <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon fa fa-group"></i> Authentication</a>
        <ul class="nav-dropdown-items">
            <li class="nav-item"><a class="nav-link" href="{{ backpack_url('user') }}"><i class="nav-icon fa fa-user"></i> <span>Users</span></a></li>
            <li class="nav-item"><a class="nav-link" href="{{ backpack_url('role') }}"><i class="nav-icon fa fa-group"></i> <span>Roles</span></a></li>
            @if(backpack_user()->can('Manage permission'))
                <li class="nav-item"><a class="nav-link" href="{{ backpack_url('permission') }}"><i class="nav-icon fa fa-key"></i> <span>Permissions</span></a></li>
            @endif
            <li class='nav-item'><a class='nav-link' href='{{ backpack_url('accountfb') }}'><i class='nav-icon fa fa-facebook'></i>Token FB</a></li>
        </ul>
    </li>
@endif

@if(backpack_user()->can('Manage content'))
    <h3 class="text-primary">Content</h3>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('page') }}'><i class='nav-icon fa fa-facebook-f'></i> Pages</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('feed') }}'><i class='nav-icon fa fa-file'></i> Feeds</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('comment') }}'><i class='nav-icon fa fa-commenting-o'></i> Comments</a></li>

@endif

<hr>
<h3 class="text-primary">Systems</h3>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('log') }}'><i class='nav-icon fa fa-terminal'></i> Logs</a></li>
