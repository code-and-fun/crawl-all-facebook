<?php

return [
   'uri' => 'https://graph.facebook.com',

   'access_token' => env('TOKEN_FB'),

    'version' => 'v3.0',

    'version_support' => ['v2.6', 'v3.0', 'v5.0', 'v6.0'],
];
